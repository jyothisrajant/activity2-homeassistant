// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// Make an InternetButton object
InternetButton button = InternetButton();

void setup() {
  // initialize the InternetButton
  button.begin();

  // Create an API endpoint to turn off lights
  // Particle.function takes 2 parameters
  // Parameter 1 = NAME OF API ENDPOINT
  // Parameter 2 = FUNCTION YOU WANT TO RUN WHEN
  //  PERSON VISITS THE ENDPOINT
  Particle.function("turnLightsOff", turnLightsOffPressed);
  Particle.function("turnLightsOn", turnLightsOnPressed);
  Particle.function("turnRainbowOn", turnRainbowOnPressed);
}

void loop() {
}

// Template of an exposed functions
int turnLightsOffPressed(String cmd) {
  button.allLedsOff();
  return 1;
}

// Turn light on
int turnLightsOnPressed(String cmd) {
  button.allLedsOn(255,255,255);
  return 1;     // 1 = success, 0 = error
}

// Turn light on
int turnRainbowOnPressed(String cmd) {
    // publish a message to the Internet!
        Particle.publish("def", "All Lights ON", 60, PRIVATE);
        button.rainbow(20);
}

